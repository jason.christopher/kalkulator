import java.util.Scanner;

public class Calc {
    public static void main(String[] args){
        double number1 = 0, number2 = 0;
        int Choice = 0;
        String Operator;

        Scanner input = new Scanner(System.in);
        do {
            System.out.println("Choose 1.Sin 2.Cos 3.Tan 4.Square 5.Rectangle 6.Triangle 7.Next 8.Exit :");
            Choice = input.nextInt();
            switch (Choice) {
                case 1:
                    System.out.println("First number: ");
                    number1 = input.nextDouble();
                    System.out.println("Sin : " + Math.toRadians(Math.sin(number1)));
                    break;
                case 2:
                    System.out.println("First number: ");
                    number1 = input.nextDouble();
                    System.out.println("Cos : " + Math.toRadians(Math.cos(number1)));
                    break;
                case 3:
                    System.out.println("First number: ");
                    number1 = input.nextDouble();
                    System.out.println("Tan : " + Math.toRadians(Math.tan(number1)));
                    break;
                case 4:
                    System.out.println("Input Length : ");
                    number1 = input.nextDouble();

                    System.out.println("Area : " + (number1 * number1));
                    System.out.println("Perimeter : " + (number1 * 4));
                    break;
                case 5:
                    System.out.println("Input Length : ");
                    number1 = input.nextDouble();

                    System.out.println("Input Height : ");
                    number2 = input.nextDouble();

                    System.out.println("Area : " + (number1 * number2));
                    System.out.println("Perimeter : " + ((number1 * 2) + (number2 * 2)));
                    break;
                case 6:
                    System.out.println("Input Base : ");
                    number1 = input.nextDouble();

                    System.out.println("Input Height : ");
                    number2 = input.nextDouble();

                    System.out.println("Input side a : ");
                    double number3 = input.nextDouble();

                    System.out.println("Input side b : ");
                    double number4 = input.nextDouble();

                    System.out.println("Area : " + ((number1 * number2) / 2));
                    System.out.println("Perimeter : " + (number1 + number3 + number4));
                    break;
                case 7:
                    System.out.println("First number: ");
                    number1 = input.nextDouble();

                    System.out.println("Second number:");
                    number2 = input.nextDouble();

                    System.out.println("Select Operator [* , / , + , -]: ");
                    Operator = input.next();


                    if (Operator.equals("+")) {
                        System.out.println("Answer : " + (number1 + number2));
                    }
                    if (Operator.equals("-")) {
                        System.out.println("Answer : " + (number1 - number2));
                    }
                    if (Operator.equals("*")) {
                        System.out.println("Answer : " + (number1 * number2));
                    }
                    if (Operator.equals("/")) {
                        if (number2 == 0) {
                            System.out.println("There is no answer");
                        } else {
                            System.out.println("Answer: " + (number1 / number2));
                        }
                    }
                    break;
            }
        } while (Choice != 8);
    }
}
